import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { PrimaryButton } from '../../components/Button/PrimaryButton';



const EditProfile = () => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [age, setAge] = useState('');

    const [foundToken, setFoundToken] = useState('');
    const [isLoad, setIsLoad] = useState(true);

    const checkToken = async () => {
        try {
            let findingToken = await AsyncStorage.getItem('@token');
            setFoundToken(findingToken);
            setIsLoad(false);
        } catch (error) {
            console.log(error);
        }
    }

    const updateProfile = async () => {
        const val = {
            age: age 
        }
        // Axios.put("https://api-nodejs-todolist.herokuapp.com/user/me", {
        //     headers: {
        //         'Authorization': `Bearer ${findingToken}`
        //     },
        //     data:val
        // }).then(result => {console.log(result.date.success)
        // }).catch(err => console.log('err: ',err))
        const req = await Axios({
            method:'POST',
            //url: `${Config.BASE_URL}/${path}`,
            url:'https://api-nodejs-todolist.herokuapp.com/user/me',
            data: val,
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${foundToken}`,
            }
          });
          console.log(req.data);
    }


    useEffect(() => {
        checkToken();
    }, [foundToken]);

    return (
        <View style={styleEdit.container}>
            <Text>Edit Profile</Text>
            {/* <TextInput placeholder='Name' style={styleEdit.textInput} value={name} onChangeText={(text) => setName(text)} />
            <TextInput placeholder='Email' style={styleEdit.textInput} value={email} onChangeText={(text) => setEmail(text)} /> */}
            <TextInput placeholder='Age' style={styleEdit.textInput} value={age} onChangeText={(value) => setAge(value)} />
            <View style={{ paddingBottom: 10, width: 300 }}>
                <TouchableOpacity style={styleEdit.containerUbah} onPress={() => updateProfile()}>
                    <Text style={styleEdit.text}>Ubah</Text>
                </TouchableOpacity>
            </View>
        </View>
    )

}

const styleEdit = StyleSheet.create({
    container: { flex: 1, padding: 20, alignItems: 'center' },
    textInput: { borderWidth: 1, paddingHorizontal: 18, width: 300, marginTop: 20 },
    containerUbah: {
        backgroundColor: 'lightblue',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 30
    },
    text: {
        fontSize: 14,
    },
})

export default EditProfile;