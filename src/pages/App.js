/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Landing from './LandingPage/Landing'
import Login from './Login/Login';
import AllTask from './AllTask/AllTask';
import ActiveTask from './ActiveTask/ActiveTask';
import CompleteTask from './CompleteTask/CompleteTask';
import Register from './Register/Register';
import Loading from './Loading/Loading';
import Home from './Home/Home';
import EditProfile from './EditProfile/EditProfile';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const App = () => {

  const [foundToken, setFoundToken] = useState('');
  const [isLoad, setIsLoad] = useState(true);

  const checkToken = async () => {
    try {
      let findingToken = await AsyncStorage.getItem('@token');
      setFoundToken(findingToken);
      setIsLoad(false);
    } catch (error) {
      console.log(error);
    }
  }

  // const loginAction = async () => {
  //   //Proses post login form untuk mendapat token/ semacamnya
  //   //let dummyToken = 'CodeSeemToken';

  //   try {
  //     await AsyncStorage.setItem('@token', dummyToken);
  //     setFoundToken(dummyToken);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  // const logoutAction = async () => {
  //   try {
  //     await AsyncStorage.removeItem('token');
  //     setFoundToken('');
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  useEffect(() => {
    checkToken();
  }, [foundToken]);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Landing' headerMode='none' >
         {/* {
          foundToken ? <Stack.Screen name="TabNavigator" component={TabNavigator} /> :
            (isLoad ? <Stack.Screen name="Loading" component={Loading}></Stack.Screen>
            :<Stack.Screen name='Login' component={Login} /> 
            )
        }  */}
        {/* <Stack.Screen name="TabNavigator" component={TabNavigator} /> */}
        <Stack.Screen name="TabNavigator" component={TabNavigator} />
        <Stack.Screen name="Loading" component={Loading}/>
        <Stack.Screen name='Login' component={Login} /> 
        <Stack.Screen name='Landing' component={Landing} />
        <Stack.Screen name='Register' component={Register} />
        <Stack.Screen name='EditProfile' component={EditProfile} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const TabNavigator = () => {
  return (
    <Tab.Navigator initialRouteName="Home" tabBarOptions={{ activeTintColor: '#e91e63', }}>
      <Tab.Screen name="Home" component={Home} options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({ color, size }) => (
          <Icon name="home" color={color} size={size} />
        ),
      }}
      />
      <Tab.Screen name="AllTask" component={AllTask} options={{
        tabBarLabel: 'All Task',
        tabBarIcon: ({ color, size }) => (
          <Icon name="home" color={color} size={size} />
        ),
      }}
      />
      <Tab.Screen name="ActiveTask" component={ActiveTask} options={{
        tabBarLabel: 'Active Task',
        tabBarIcon: ({ color, size }) => (
          <Icon name="account" color={color} size={size} />
        ),
      }}
      />
      <Tab.Screen name="CompleteTask" component={CompleteTask} options={{
        tabBarLabel: 'Completed Task',
        tabBarIcon: ({ color, size }) => (
          <Icon name="account" color={color} size={size} />
        ),
      }}
      />
    </Tab.Navigator>
  );
};

export default App;
