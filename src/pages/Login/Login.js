import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TextInput, Button, TouchableOpacity } from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';


const LoginPage = (props) => {

  const [email, setEmail] = useState('irfansaputra@gmail.com');
  const [password, setPassword] = useState('12345678');
  
  const submitLogin = async (val) => {
    try {
      const response = await Axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', val);
      // const response = await Axios({
      //   method:'GET',
      //   //url: `${Config.BASE_URL}/${path}`,
      //   url:'https://api-nodejs-todolist.herokuapp.com/user/login',
      //   data: val,
      //   headers:{
      //       'Accept': 'application/json',
      //       'Content-Type': 'application/json',
      //   }
      // });
      if(response.data.token){
        AsyncStorage.setItem('@token', response.data.token);
        props.navigation.navigate('TabNavigator');
      }
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <Text style={{marginTop:150}}>Welcome to To-DO List App</Text>
      <TextInput title='username' style={styles.textinput} placeholder='Masukan username' 
        value={email} onChangeText={(value) => setEmail(value)}/>
      <TextInput title='password' style={styles.textinput} placeholder='Masukan Password' 
        value={password} onChangeText={(value) => setPassword(value)}/>
      <TouchableOpacity style={styles.appButtonContainer} onPress={() => submitLogin({email : email, password: password})}>
        <Text style={styles.appButtonText}>Login</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  textinput: { marginTop: 20, width: 327, height: 50, borderWidth: 2, borderRadius: 50 / 2, textAlign: 'center' },
  loginbutton: { marginTop: 20, width: 327, height: 50, borderWidth: 2, borderRadius: 50 / 2, },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 12,
    width: 327,
    marginTop: 20
  },
  appButtonText: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }
})

export default LoginPage;