import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';



const Home = (props) => {
    const { navigation } = props;

    const [foundToken, setFoundToken] = useState('');
    const [isLoad, setIsLoad] = useState(true);
    const [dataUser, setdataUser] = useState({
        name: '',
        email: '',
        age: ''
    }
    );
    const [dataAllTask, setdataAllTask] = useState({ count: '', });
    const [dataCompleteTask, setdataCompleteTask] = useState({ count: '', });

    const checkToken = async () => {
        try {
            let findingToken = await AsyncStorage.getItem('@token');
            if(findingToken){
                setFoundToken(findingToken);
            }
            return findingToken;
            setIsLoad(false);
        } catch (error) {
            console.log(error);
        }
    }

    const logout = async () => {
        try {
            await AsyncStorage.removeItem('@token');
            props.navigation.navigate('Login');
        }
        catch (exception) {
            return false;
        }
    }

    useEffect(() => {
        checkToken();
        // let findingToken = AsyncStorage.getItem('@token');
        // setFoundToken(findingToken);
        if (foundToken){
            //console.log(findingToken)
            
            getProfile();
            getCountAllTask();
            getCountCompleteTask();
        }else{
            alert('token tidak ada')
        }
        //then({getProfile})
        //then({ getCountAllTask})
        //then({getCountCompleteTask})
        //getProfile();
        //getCountAllTask();
        //getCountCompleteTask();
    }, [foundToken]);

    useEffect(()=>{
        const unsubscribe = navigation.addListener('focus', () => {
            // The screen is focused
            //inputSearch.current.focus();
            console.log('masuk ke addlistener');
            getProfile();
            getCountAllTask();
            getCountCompleteTask();
          });
          return(unsubscribe);
    },[])

    const getProfile = async () => {
        try {
            const req = await Axios({
                method:'GET',
                //url: `${Config.BASE_URL}/${path}`,
                url:'https://api-nodejs-todolist.herokuapp.com/user/me',
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${foundToken}`,
                }
              });
              setdataUser(req.data)
        //     Axios.get('https://api-nodejs-todolist.herokuapp.com/user/me', {
        //         headers: {
        //             'Authorization': `Bearer ${foundToken}`
        //         }
        //     }).then(result => {
        //         setdataUser(result.data);
        //     }).catch(err => console.log('err getProfile: ', err))
         } catch (exception) {
             return false;
        }
    }

    const getCountAllTask = async () => {
        try {
            Axios.get('https://api-nodejs-todolist.herokuapp.com/task', {
                headers: {
                    'Authorization': `Bearer ${foundToken}`
                }
            }).then(result => {
                setdataAllTask(result.data);
            }).catch(err => console.log('err get count all task: ', err));
            // const req = await Axios({
            //     method:'GET',
            //     //url: `${Config.BASE_URL}/${path}`,
            //     url:'https://api-nodejs-todolist.herokuapp.com/task',
            //     headers:{
            //         'Accept': 'application/json',
            //         'Content-Type': 'application/json',
            //         'Authorization': `Bearer ${foundToken}`,
            //     }
            //   });
              //setdataAllTask(req.data)
              //console.log(req.data);
        } catch (exception) {
            return false;
        }
    }

    const getCountCompleteTask = async () => {
        try {
            Axios.get('https://api-nodejs-todolist.herokuapp.com/task?completed=true', {
                headers: {
                    'Authorization': `Bearer ${foundToken}`
                }
            }).then(result => {
                setdataCompleteTask(result.data)
            }).catch(err => ('err get task complete:', err));
        } catch (exception) {
            return false;
        }
    }


    return (
        <View style={styleHome.container}>

            <Image source={{ uri: 'https://reactjs.org/logo-og.png' }} style={styleHome.avatar} />
            <Text style={styleHome.textBold}>{dataUser.name} ({dataUser.age}th)</Text>
            <Text style={styleHome.textUser}>{dataUser.email}</Text>

            <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
                <Text style={styleHome.textBold}>Edit Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={logout}>
                <Text style={styleHome.textBold}>Logout</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 30 }}>
                <View style={styleHome.viewTask}>
                    <Text> All Tasks</Text>
                    <Text style={styleHome.textTask}>{dataAllTask.count}</Text>
                </View>
                <View style={styleHome.viewTask}>
                    <Text> Completed Tasks</Text>
                    <Text style={styleHome.textTask}>{dataCompleteTask.count}</Text>
                </View>
            </View>
        </View>
    )
}

const styleHome = StyleSheet.create({
    container: { padding: 20, alignItems: 'center', flex: 1 },
    textUser: { textAlign: 'center' },
    textBold: { textAlign: 'center', fontWeight: 'bold', marginTop: 10 },
    viewTask: { alignItems: 'center', flex: 1 },
    textTask: { fontSize: 30, fontWeight: 'bold' },
    avatar: { height: 80, width: 80, borderRadius: 80 }
}
)

export default Home;