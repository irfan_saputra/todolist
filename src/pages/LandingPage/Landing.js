/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

import { PrimaryButton } from '../../components/Button/PrimaryButton';

import styles from './LandingStyle';

import imgBackground from '../../../src/assets/backgroundimg.jpeg';
const image = {uri: 'https://reactjs.org/logo-og.png'};

const { height } = Dimensions.get('window');

function Landing(props) {
  const { navigation } = props;
  return (
    <View style={styles.container}>
      <ImageBackground source={imgBackground} style={styles.imageBackground}>
        <View style={{alignItems: 'center', paddingTop: height * 0.2}}>
          <Image source={image} style={styles.img} />

          <Text style={styles.title}>
            Aplikasi untuk mengatur agendamu
          </Text>
          <Text style={styles.subtitle}>
            Redu App dapat menjadi sahabat terbaik kamu dalam mengatur agendamu yang berfaedah dan penuh hikmah
          </Text>
        </View>

        <View style={styles.container}>
          <View style={{ paddingBottom: 10, width: 300 }}>
                <TouchableOpacity style={styleLanding.containerUbah} title="Login" onPress={() => navigation.navigate('Login')}>
                    <Text style={styleLanding.text}>Login</Text>
                </TouchableOpacity>
            </View>
            <View style={{ paddingBottom: 10, width: 300 }}>
                <TouchableOpacity style={styleLanding.containerUbah} title="Register" onPress={() => navigation.navigate('Register')}>
                    <Text style={styleLanding.text}>Register</Text>
                </TouchableOpacity>
            </View>
        </View>
      </ImageBackground>
    </View>
  )
}

const styleLanding = StyleSheet.create({
    container: { flex: 1, padding: 20, alignItems: 'center' },
    textInput: { borderWidth: 1, paddingHorizontal: 18, width: 300, marginTop: 20 },
    containerUbah: {
        backgroundColor: 'lightblue',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 30
    },
    text: {
        fontSize: 14,
    },
})

export default Landing;
