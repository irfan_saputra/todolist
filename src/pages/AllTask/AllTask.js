import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView, Touchable } from 'react-native'
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';



const AllTask = (props) => {
    const { navigation } = props;

    const [foundToken, setFoundToken] = useState('');
    const [desc, setDesc] = useState('');
    const [users, setTaskData] = useState([]);
    //const [description,setDescription] = useState('')


    //const [isLoad, setIsLoad] = useState(true);
    useEffect(() => {
        initAllTaskData();
    }, []);

    // useEffect(()=>{
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         console.log('get all task')
    //         getAllTask();
    //       });
    //       return(unsubscribe);
    // },[])

    const Item = ({...item}) => {
        return (
            <View style={styleAllTask.containerItem}>
                    {item.completed ? <Ionicons name="checkmark-circle-outline" size={20} color="tomato" /> : <Ionicons name="list-circle-outline" size={20} color="tomato" />}
                    <Text style={styleAllTask.text}>{item.description}</Text>
                    <Text style={styleAllTask.text}>{item.completed}</Text>
                    {/* <Text style={styleAllTask.text}>{createdate}</Text>
                <Text style={styleAllTask.text}>{updatedate}</Text> */}
                 <View style={{flexDirection:'row'}}>
                    <TouchableOpacity
                        style={{ flex:1,width:75 ,backgroundColor: "tomato" }}
                        onPress={() => updateTask(item._id, !item.completed)}
                    >
                        <Text style={styleAllTask.text}>Change Status</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flex:1,width:75, backgroundColor: "red" }}
                        onPress={() => deleteTask(item._id)}
                    >
                        <Text style={styleAllTask.text}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    const initAllTaskData = async () => {
        const token = await checkToken();
        setFoundToken(token);
        getAllTask(token);
    }

    const checkToken = async () => {
        // try {
        //     let findingToken = await AsyncStorage.getItem('@token');
        //     setFoundToken(findingToken);
        // } catch (error) {
        //     console.log(error);
        // }
        try {
            const token = await AsyncStorage.getItem('@token');

            if (token) return token;
            else return '';
        } catch (e) {
            alert(e);
        }
    }

    const addTask = async () => {
        const val = {
            description: desc
        }
        console.log(`token ada : ${foundToken}`);
        console.log(`val ada : ${val}`);

        try {
            const req = await axios({
                method: 'post',
                //url: `${Config.BASE_URL}/${path}`,
                url: 'https://api-nodejs-todolist.herokuapp.com/task',
                data: val,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${foundToken}`,
                }
            });
            //console.log(req.data);
            setDesc('');
            getAllTask(foundToken);
        } catch (err) {
            console.log(err);
        }
    }

    const getAllTask = async (foundToken) => {
        axios({
            method: 'get',
            url: 'https://api-nodejs-todolist.herokuapp.com/task',
            headers: {
                'Authorization': `Bearer ${foundToken}`,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                // handle success
                setTaskData(response.data.data);
                //setLoading(false);
            })
            .catch((error) => {
                // handle error
                alert(error);
                //setLoading(false);
            })
    }

    const updateTask = async (id, isCompleted) => {
        //setLoading(true);
        const token = await checkToken();
        axios({
            method: 'put',
            url: `https://api-nodejs-todolist.herokuapp.com/task/${id}`,
            headers: {
                'Authorization': `Bearer ${foundToken}`,
                'Content-Type': 'application/json'
            },
            data: {
                completed: isCompleted
            }
        })
            .then((response) => {
                // handle success
                //setLoading(false);
                initAllTaskData();
            })
            .catch((error) => {
                // handle error
                alert(error);
                //setLoading(false);
            })
    }

    const deleteTask = async (id) => {
        setLoading(true);
        const token = await checkToken();
        axios({
            method: 'delete',
            url: `https://api-nodejs-todolist.herokuapp.com/task/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                // handle success
               // setLoading(false);
                initAllTaskData();
            })
            .catch((error) => {
                // handle error
                alert(error);
                //setLoading(false);
            })
    }

    return (
        <View style={styleAllTask.container}>
            <ScrollView>
                <Text>Masukan Task Baru</Text>
                <TextInput placeholder="Description" style={styleAllTask.textinput} value={desc} onChangeText={(value) => setDesc(value)}></TextInput>
                <View style={{ paddingBottom: 10, width: 300 }}>
                    <TouchableOpacity style={styleAllTask.containerUbah} onPress={addTask}>
                        <Text style={styleAllTask.text}>Tambah</Text>
                    </TouchableOpacity>
                </View>
                <Text>All Tasks</Text>
                {users ? users.map((user) => {
                    return <Item {...user} key={user._id}/>
                }) : console.log('data kosong')}
            </ScrollView>
        </View>

    )
}

const styleAllTask = StyleSheet.create({
    container: { padding: 20, alignItems: 'center', flex: 1 },
    textinput: { borderWidth: 1, width: 300, marginTop: 10, borderRadius: 50, textAlign: "center" },
    containerUbah: {
        backgroundColor: 'lightblue',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 10
    },
    line: { height: 2, backgroundColor: 'black', marginVertical: '10' },
    text: {
        fontSize: 14,
    },
    containerItem: { padding: 10, borderWidth: 1, width: 300 },
})

export default AllTask;