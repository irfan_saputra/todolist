import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import styles from './PrimaryButton-Style';

const PrimaryButton = ({title, ...others}) => {
  return (
    <TouchableOpacity style={styles.container} {...others}>
      <Text style={styles.text}>{title.toUpperCase()}</Text>
    </TouchableOpacity>
  );
};

export default PrimaryButton;
